{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Party where

import Employee
import Data.List
import Data.Tree
--import Data.Monoid

-- Exercise 1
glCons :: Employee -> GuestList -> GuestList
glCons e (GL es f) = GL (e:es) $ (empFun e) + f

instance Monoid GuestList where
  mempty = GL [] 0
  mappend (GL a x) (GL b y) = GL (a ++ b) (x + y)

moreFun :: GuestList -> GuestList -> GuestList
moreFun a@(GL _ af) b@(GL _ bf) | af >= bf  = a
                                | otherwise = b

-- Exercise 2
treeFold :: (a -> [b] -> b) -> Tree a -> b
treeFold f (Node a []) = f a []
treeFold f (Node a ts) = f a $ map (treeFold f) ts

-- Exercise 3
nextLevel :: Employee -> [(GuestList, GuestList)] -> (GuestList, GuestList)
nextLevel e r = (mconcat wb, glCons e (mconcat nb))
              where (wb,nb) = unzip r

-- Exercise 4
maxFun :: Tree Employee -> GuestList
maxFun = uncurry max . (treeFold nextLevel)

testList1 :: GuestList
testList1 = maxFun testCompany

testList2 :: GuestList
testList2 = maxFun testCompany2

-- Exercise 5
main = do
  contents <- readFile "company.txt"
  let tree = read contents
      list = maxFun tree
  putStrLn (formatGL list)

formatGL :: GuestList -> String
formatGL (GL es fun) = "Total fun: " ++ show fun ++ "\n" ++ formatEmpList es

formatEmpList :: [Employee] -> String
formatEmpList = unlines . sort . (map empName)
