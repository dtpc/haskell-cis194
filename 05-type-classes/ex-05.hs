{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

import ExprT
import Parser
import qualified Data.Map as M
import qualified StackVM as VM

-- Exercise 1
eval :: ExprT -> Integer
eval (Lit i) = i
eval (Add a b) = (eval a) + (eval b)
eval (Mul a b) = (eval a) * (eval b)

-- Exercise 2
evalStr :: String -> Maybe Integer
evalStr s = eval <$> (parseExp Lit Add Mul s)

-- Exercise 3
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a
  
instance Expr ExprT where
  lit = Lit
  add = Add
  mul = Mul

reify :: ExprT -> ExprT
reify = id

-- Exercise 4
instance Expr Integer where
  lit = id
  add = (+)
  mul = (*)

instance Expr Bool where
  lit = (>0)
  add = (||)
  mul = (&&)

newtype MinMax = MinMax Integer 
  deriving (Eq, Show)

instance Expr MinMax where
  lit = MinMax
  add (MinMax x) (MinMax y) = lit $ min x y
  mul (MinMax x) (MinMax y) = lit $ max x y

newtype Mod7 = Mod7 Integer 
  deriving (Eq, Show)

instance Expr Mod7 where
  lit = Mod7 . (`mod` 7)
  add (Mod7 x) (Mod7 y) = lit (x + y)
  mul (Mod7 x) (Mod7 y) = lit (x * y) 


-- Exercise 5
instance Expr VM.Program where
  lit i = [VM.PushI i]
  add x y = x ++ y ++ [VM.Add]
  mul x y = x ++ y ++ [VM.Mul]

compile :: String -> Maybe VM.Program
compile = parseExp lit add mul

ex5 :: Maybe (Either String VM.StackVal)
ex5 = VM.stackVM <$> compile "4 + (6 * 2)"


-- Exercise 6
class HasVars a where
  var :: String -> a

data VarExprT = VLit Integer
              | VAdd VarExprT VarExprT
              | VMul VarExprT VarExprT
              | VVar String 
  deriving (Show, Eq)

instance Expr VarExprT where
  lit = VLit
  add = VAdd
  mul = VMul

instance HasVars VarExprT where
  var = VVar

instance HasVars (M.Map String Integer -> Maybe Integer) where
  var = M.lookup

instance Expr (M.Map String Integer -> Maybe Integer) where
  lit = const . Just
  add x y m = (+) <$> (x m) <*> (y m)
  mul x y m = (*) <$> (x m) <*> (y m)

withVars :: [(String, Integer)]
         -> (M.Map String Integer -> Maybe Integer )
         -> Maybe Integer
withVars vs expr = expr $ M.fromList vs

ex6 :: Maybe Integer
ex6 = withVars [("x",5),("y",6)] $ mul (var "x") (add (var "y") (var "x"))
