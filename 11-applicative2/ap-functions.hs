{-# OPTIONS_GHC -Wall #-}

-- Exercises at end of week 11 lecture

import Control.Applicative hiding ((*>))

(*>) :: Applicative f => f a -> f b -> f b
(*>) = flip const

mapA :: Applicative f => (a -> f b) -> ([a] -> f [b])
mapA f = sequenceA . fmap f

sequenceA :: Applicative f => [f a] -> f [a]
sequenceA []     = pure []
sequenceA (x:xs) = liftA2 (:) x (sequenceA xs)

replicateA :: Applicative f => Int -> f a -> f [a]
replicateA i a = replicate i <$> a 
