module Golf where

import Data.List

-- Exercise 1
nth :: Int -> [a] -> [a]
nth n l = case drop (n-1) l of
          (x:xs) -> x : nth n xs
          [] -> []

skips :: [a] -> [[a]]
skips l = map (flip nth l) [1..(length l)]

-- Exercise 2
localMaxima :: [Integer] -> [Integer]
localMaxima (x:xs@(y:z:_)) 
        | y > x && y > z = y : localMaxima xs 
        | otherwise      =     localMaxima xs
localMaxima _ = []

-- Exercise 3
histogram :: [Integer] -> String
histogram l = let
                s = map(\n -> ['*' | x <- l, x==n]) [0..9]
                m = maximum $ map length s
                p n xs = replicate (max 0 (n - length xs)) ' ' ++ xs
                h = map (p m) s
              in
                unlines (transpose $ h) ++ "==========\n0123456789"

