{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Scrabble where

import Data.Char
import Data.Monoid

-- Exercise 3
newtype Score = Score Int
  deriving (Eq,Show,Ord,Num)

getScore :: Score -> Int
getScore (Score s) = s

instance Monoid Score where
  mempty  = 0
  mappend = (+)

score :: Char -> Score
score c | x `elem` "AEILNORSTU" = Score 1
        | x `elem` "DG"         = Score 2
        | x `elem` "BCMP"       = Score 3
        | x `elem` "FHVWY"      = Score 4
        | x `elem` "K"          = Score 5
        | x `elem` "JX"         = Score 8
        | x `elem` "QZ"         = Score 10
        | otherwise             = 0
        where x = toUpper c

scoreString :: String -> Score
scoreString = mconcat . map score

