{-# OPTIONS_GHC -Wall #-}

-- From http://blog.sigfpe.com/2009/01/haskell-monoids-and-their-uses.html

import Data.Monoid
import Data.Foldable
import Control.Monad.Writer
import Control.Monad.State

fact1 :: Integer -> Writer String Integer
fact1 0 = return 1
fact1 n = do
  let n' = n-1
  tell $ "Take 1 away from " ++ show n ++ "\n"
  m <- fact1 n'
  tell $ "Call f " ++ show m ++ "\n"
  let r = n*m
  tell $ "Multiply " ++ show n ++ " and " ++ show m ++ "\n"
  return r
