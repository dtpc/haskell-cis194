{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

newtype And = And Bool
  deriving (Show, Eq)

getAnd :: And -> Bool
getAnd (And b) = b

instance Monoid (And) where
  mempty = And True
  mappend (And a) (And b) = And (a && b)


newtype Or = Or Bool
  deriving (Show, Eq)

getOr :: Or -> Bool
getOr (Or b) = b

instance Monoid (Or) where
  mempty = Or False
  mappend (Or a) (Or b) = Or (a || b)


-- Does this need to be wrapped?
newtype F a = F (a -> a)

getF :: F a -> a -> a
getF (F a) = a

instance Monoid (F a)  where
  mempty = F id
  mappend (F a) (F b) = F (b . a)

g = getF . mconcat . map F $ [(`mod` 4), (*2), (+4), negate]

