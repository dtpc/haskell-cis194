{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances, TypeSynonymInstances #-}

module JoinList where

import Data.Monoid
import Sized
import Scrabble
import Buffer
import Editor

data JoinList m a = Empty
                  | Single m a
                  | Append m (JoinList m a) (JoinList m a)
  deriving (Eq, Show)

-- Exercise 1
(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
(+++) x y = Append ((tag x) <> (tag y)) x y 

tag :: Monoid m => JoinList m a -> m
tag Empty          = mempty
tag (Single m _)   = m
tag (Append m _ _) = m

listToJlSize :: [a] -> JoinList Size a
listToJlSize []     = Empty
listToJlSize (x:xs) = Single (Size 1) x +++ listToJlSize xs

jlist :: JoinList Size Integer
jlist = listToJlSize [1..10]

-- Exercise 2
-- 1
indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a
indexJ _ Empty      = Nothing
indexJ n _ | n < 0  = Nothing
indexJ n (Single _ a) | n == 0    = Just a
                      | otherwise = Nothing
indexJ n (Append s l r) | n < sl    = indexJ n l
                        | n < ss    = indexJ (n - sl) r
                        | otherwise = Nothing
                        where 
                          ss = getSize $ size $ s
                          sl = getSize $ size $ (tag l)

(!!?) :: [a] -> Int -> Maybe a
[]     !!? _          = Nothing
_      !!? i | i < 0  = Nothing
(x:_)  !!? 0          = Just x
(_:xs) !!? i          = xs !!? (i-1)

jlToList :: JoinList m a -> [a]
jlToList Empty            = []
jlToList (Single _ a)     = [a]
jlToList (Append _ l1 l2) = jlToList l1 ++ jlToList l2

-- 2
dropJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
dropJ _ Empty          = Empty 
dropJ n l | n <= 0     = l
dropJ _ (Single _ _)   = Empty
dropJ n (Append s l r) | n < sl    = (dropJ n l) +++ r
                       | n < ss    = dropJ (n-sl) r
                       | otherwise = Empty
                        where 
                          ss = getSize $ size $ s
                          sl = getSize $ size $ (tag l)

-- 3
takeJ :: (Sized b, Monoid b) => Int -> JoinList b a -> JoinList b a
takeJ _ Empty          = Empty 
takeJ n _ | n <= 0     = Empty
takeJ _ l@(Single _ _) = l
takeJ n a@(Append s l r) | n < sl    = (takeJ n l)
                         | n < ss    = l +++ takeJ (n-sl) r
                         | otherwise = a
                        where 
                          ss = getSize $ size $ s
                          sl = getSize $ size $ (tag l)

-- Exercise 3
scoreLine :: String -> JoinList Score String
scoreLine s = Single (scoreString s) s

-- Exercise 4
instance Buffer (JoinList (Score,Size) String) where
  toString = unlines . jlToList 
  fromString = (foldr (+++) Empty) . (map lineToJlSS) . lines
  line = indexJ
  replaceLine i _ b | i < 0 || i > n = b
                    where n = (numLines b) - 1
  replaceLine i s b = before +++ (lineToJlSS s) +++ after
                    where before = takeJ i b
                          after  = dropJ (i+1) b
  numLines = getSize . snd . tag
  value = getScore . fst . tag

lineToJlSS :: String -> JoinList (Score,Size) String
lineToJlSS [] = Empty
lineToJlSS s  = Single (scoreString s, Size 1) s


tmpBuf :: JoinList (Score,Size) String
tmpBuf = fromString $ unlines
         [ "This buffer is for notes you don't want to save, and for"
         , "evaluation of steam valve coefficients."
         , "To load a different file, type the character L followed"
         , "by the name of the file."
         ]

main = runEditor editor $ tmpBuf

