{-# OPTIONS_GHC -Wall #-}

-- Exercise 1
-- 1                                                                             
fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x:xs)
  | even x    = (x - 2) * fun1 xs
  | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even

--2
fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n | even n    = n + fun2 (n `div` 2)
       | otherwise = fun2 (3 * n + 1)

fun2' :: Integer -> Integer
fun2' = sum 
      . filter even
      . takeWhile (/=1) 
      . iterate (\n -> if even n then n `div` 2 else 3*n+1)


-- Exercise 2
data Tree a = Leaf
            | Node Integer (Tree a) a (Tree a)
              deriving (Show, Eq)

{-
insert :: a -> Tree a -> Tree a
insert x Leaf = Node 0 Leaf x Leaf
insert x (Tree n left a right) = 

foldTree :: [a] -> Tree a
-}

-- Exercise 3
xor :: [Bool] -> Bool
xor = foldr (\a b -> if a then (not b) else b ) False

map' :: (a -> b) -> [a] -> [b]
map' f = foldr ( (:) . f ) [] 

--myFoldl :: (a -> b -> a) -> a -> [b] -> a
--myFoldl f base xs = foldr

-- Exercise 4

cartProd :: [a] -> [b] -> [(a,b)]
cartProd xs ys = [ (x,y) | x <- xs, y <-ys ]

sieveSundaram :: Integer -> [Integer]
sieveSundaram n = let
    ns = [1..n] 
    ps = filter (uncurry (>=)) $ cartProd ns ns
    vs = filter (<n) $ map (\(i,j) -> i+j+2*i*j) $ ps 
    xs = filter ( not . (`elem` vs) ) ns
  in
    map ((+1).(*2)) xs

