import Data.Char

-- Credit card validation

-- Exercise 1
toDigits :: Integer -> [Integer]
toDigits n
  | n > 0 = map (toInteger . digitToInt) (show n)
  | otherwise = []

toDigitsRev :: Integer -> [Integer]
toDigitsRev n = reverse (toDigits n)

-- Exercise 2
doubleEveryOtherL (x:y:xs) = x : 2*y : (doubleEveryOtherL xs)
doubleEveryOtherL x = x

doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther = reverse . doubleEveryOtherL . reverse

-- Exervice 3
sumDigits :: [Integer] -> Integer
sumDigits (x:xs) = sum(toDigits x) + sumDigits(xs)
sumDigits [] = 0

-- Exercise 4
validate :: Integer -> Bool
validate n = (sumDigits . doubleEveryOther . toDigits) n `mod` 10 == 0
-- pointfree
-- validate = ((==) 0) . (flip mod 10) . sumDigits . doubleEveryOther . toDigits


-- Towers of Hanoi

-- Exercise 5
type Peg = String
type Move = (Peg, Peg)
hanoi :: Integer -> Peg -> Peg -> Peg -> [Move]
hanoi 1 a b c = [(a,c)] 
hanoi n a b c = (hanoi (n-1) a b c) ++ [(a,b)] ++ (hanoi (n-1) c a b)



