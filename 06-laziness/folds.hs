{-# OPTIONS_GHC -Wall #-}

-- https://wiki.haskell.org/Foldr_Foldl_Foldl'

import Prelude hiding (foldr,foldl)

veryBigList :: [Integer]
veryBigList = [1..100000000]

foldr :: (a -> b -> b) -> b -> [a] -> b
foldr f z []     = z
foldr f z (x:xs) = x `f` foldr f z xs 

sum1 :: Num a => [a] -> a
sum1 = foldr (+) 0

try1 :: Integer
try1 = sum1 veryBigList
-- *** Exception: stack overflow

foldl :: (b -> a -> b) -> b -> [a] -> b
foldl f z []     = z
foldl f z (x:xs) = foldl f (f z x) xs

sum2 :: Num a => [a] -> a
sum2 = foldl (+) 0

try2 :: Integer
try2 = sum2 veryBigList
-- *** Exception: stack overflow

foldl' :: (b -> a -> b) -> b -> [a] -> b
foldl' f z []     = z
foldl' f z (x:xs) = seq z $ foldl' f (f z x) xs

sum3 :: Num a => [a] -> a
sum3 = foldl' (+) 0

try3 :: Integer
try3 = sum3 veryBigList


(?) :: Int -> Int -> Int
_ ? 0 = 0
x ? y = x*y

list :: [Int]
list = [2,3,undefined,5,0]

ok :: Int
ok = foldl (?) 1 list

boom :: Int
boom = foldl' (?) 1 list

test :: Int
test = foldr (?) 1 list

-- mutually recursive fold for no good reason ...
foldx :: (a -> b -> b) -> b -> [a] -> b
foldx f z []     = z
foldx f z (x:xs) = x `f` foldy (flip f) z xs 

foldy :: (b -> a -> b) -> b -> [a] -> b
foldy f z []     = z
foldy f z (x:xs) = seq z $ foldx (flip f) (f z x) xs

