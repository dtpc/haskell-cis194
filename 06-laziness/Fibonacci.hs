{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}

-- Exercise 1
fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fibs1 :: [Integer]
fibs1 = map fib [0..]

-- Exercise 2
fibs2 :: [Integer]
fibs2 = let f = \a b -> b : f b (a + b)
        in 0 : f 0 1
        
-- Exercise 3
data Stream a = Stream a (Stream a)

streamToList :: Stream a -> [a]
streamToList (Stream a s) = a : streamToList s

instance Show a => Show (Stream a) where
  show = show . take 20 . streamToList

-- Exercise 4
streamRepeat :: a -> Stream a
streamRepeat a = Stream a (streamRepeat a)

streamMap :: (a -> b) -> Stream a -> Stream b
streamMap f (Stream a s) = Stream (f a) (streamMap f s) 

streamFromSeed :: (a -> a) -> a -> Stream a
streamFromSeed f a = Stream a (streamFromSeed f (f a))

-- Exercise 5
nats :: Stream Integer
nats = streamFromSeed (+1) 0

interleaveStreams :: Stream a -> Stream a -> Stream a
interleaveStreams (Stream a as) b = Stream a (interleaveStreams b as)

ruler :: Stream Integer
ruler = f 0 
      where f n = interleaveStreams (streamRepeat n) (f (n+1))

-- Exercise 6
x :: Stream Integer
x = Stream 0 (Stream 1 (streamRepeat 0))

instance Num (Stream Integer) where
  fromInteger n = Stream n (streamRepeat 0)
  negate (Stream a s) = Stream (-a) (negate s)
  (+) (Stream a0 a') (Stream b0 b') = Stream (a0 + b0) (a' + b')
  (*) (Stream a0 a') b@(Stream b0 b') = Stream (a0 * b0) $ (streamMap (*a0) b') + (a' * b)

instance Fractional (Stream Integer) where
  (/) a@(Stream a0 a') b@(Stream b0 b') = Stream (a0 `div` b0) $ streamMap (`div` b0) (a' - (a/b)*b')

fibs3 :: Stream Integer
fibs3 = x / ( (fromInteger 1) - x - (x^2) )
         
-- Exercise 7
data Matrix = Matrix Integer Integer Integer Integer deriving (Eq)
instance Show Matrix where
  show (Matrix a b c d) = concat ["[",show a,",",show b,"\n ",show c,",",show d,"]"] 

instance Num Matrix where
  fromInteger n = Matrix n n n n   

  (+) (Matrix a1 b1 c1 d1) (Matrix a2 b2 c2 d2) = 
        Matrix (a1+a2) (b1+b2) (c1*c2) (d1*d2)

  (-) (Matrix a1 b1 c1 d1) (Matrix a2 b2 c2 d2) = 
        Matrix (a1-a2) (b1-b2) (c1-c2) (d1-d2)

  (*) (Matrix a1 b1 c1 d1) (Matrix a2 b2 c2 d2) = 
        Matrix (a1*a2 + b1*c2) (a1*b2+b1*d2) (c1*a2 + d1*c2) (c1*b2+d1*d2) 
    
  negate (Matrix a b c d) = Matrix (-a) (-b) (-c) (-d)

fibs4 :: Integer -> Integer
fibs4 0 = 0
fibs4 n = case e of
            Matrix f _ _ _ -> f
          where e = (Matrix 1 1 1 0) ^ (n-1)

