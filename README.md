CIS 194: Introduction to Haskell (Spring 2013)
==============================================

http://www.seas.upenn.edu/~cis194/spring13/

Course Description
------------------

Haskell is a high-level, pure functional programming language with a strong static type system and elegant mathematical underpinnings, and is being increasingly used in industry by organizations such as Facebook, AT&T, and NASA. In the first 3/4 of the course, we will explore the joys of pure, lazy, typed functional programming in Haskell (warning: euphoric, mind-bending epiphanies may result!). The last 1/4 of the course will consist of case studies in fun and interesting applications of Haskell. Potential case study topics include automated randomized testing, software transactional memory, graphics generation, parser combinators, or others as time and interest allow. Evaluation will be based on class participation, weekly programming assignments, and an open-ended final project.

Reading
-------

[Real World Haskell](http://book.realworldhaskell.org/), by Bryan O’Sullivan, Don Stewart, and John Goerzen, published by O’Reilly. A thorough and detailed introduction to Haskell that gets into the nitty gritty of using Haskell effectively in the “real world”. Can be read online for free, or in dead tree form.

[Learn You a Haskell for Great Good!](http://learnyouahaskell.com/) is a whimsical and easy-to-follow Haskell tutorial, with super awesome illustrations. Also available online or in dead tree form.

The [Haskell wikibook](http://en.wikibooks.org/wiki/Haskell) actually contains a substantial amount of well-written information; a great resource if you’re having trouble understanding a particular topic and want a different approach.

The [Haskell wiki](http://www.haskell.org/) is a huge grab-bag of all sorts of information, examples, explanations. The quality varies but it’s definitely a great resource.

The [Typeclassopedia](http://haskell.org/haskellwiki/Typeclassopedia) explains many of the type classes in the standard libraries (Functor, Applicative, Monad, Monoid, Arrow, Foldable, Traversable…).

[Planet Haskell](http://planet.haskell.org/) aggregates blog posts from the Haskell community.

There is a [Haskell subreddit](http://www.reddit.com/r/haskell/) for aggregating Haskell-related websites, blog posts, and news.

Reference
---------

[Standard library documentation](http://www.haskell.org/ghc/docs/latest/html/libraries/index.html).

A useful [Haskell cheatsheet](http://cheatsheet.codeslower.com/).

