{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Risk where

--import System.Random
import Data.List
import Control.Monad
import Control.Monad.Random

------------------------------------------------------------
-- Die values

newtype DieValue = DV { unDV :: Int } 
  deriving (Eq, Ord, Show, Num)

first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

instance Random DieValue where
  random           = first DV . randomR (1,6)
  randomR (low,hi) = first DV . randomR (max 1 (unDV low), min 6 (unDV hi))

die :: Rand StdGen DieValue
die = getRandom

------------------------------------------------------------
-- Risk

type Army = Int

data Battlefield = Battlefield { attackers :: Army, defenders :: Army }
  deriving (Eq,Show)

-- Exercise 2
roll :: Int -> Rand StdGen [DieValue]
roll n = replicateM n die >>= return . sortBy (flip compare)

battle :: Battlefield -> Rand StdGen Battlefield
battle (Battlefield a d) = do
  let na = min (a - 1) 3
  let nd = min d 2
  ra <- roll na
  rd <- roll nd 
  let res = zipWith (>) ra rd
  let aWins = length $ filter id res
  let dWins = length res - aWins
  return (Battlefield (a - dWins) (d - aWins))

-- Exercise 3
invade :: Battlefield -> Rand StdGen Battlefield
invade b@(Battlefield a d) 
  | (a < 2) || (d < 1) = return b
  | otherwise          = battle b >>= invade

-- Exercise 4
successProb :: Battlefield -> Rand StdGen Double
successProb b = do
  bfs <- replicateM 1000 $ invade b
  let res = map (\(Battlefield _ d) -> d == 0) bfs
  let aWins = length $ filter id res
  return $ fromIntegral aWins / fromIntegral (length res)

