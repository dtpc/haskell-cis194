{-# OPTIONS_GHC -Wall #-}

module LogAnalysis where

import Log
import Data.Char

-- Ex 1
parseInt :: String -> Maybe Int
parseInt s@(_:_) | foldr ((&&) . isDigit) True s = Just (read s)
parseInt _ = Nothing

parseMsgType :: [String] -> (Maybe MessageType, [String])
parseMsgType ("I":xs) = (Just Info,xs)
parseMsgType ("W":xs) = (Just Warning,xs)
parseMsgType ("E":x:xs) = (Error <$> parseInt x, xs)
parseMsgType _ = (Nothing,[])

parseTime :: [String] -> (Maybe TimeStamp, [String])
parseTime (x:xs) = (parseInt x, xs)
parseTime _ = (Nothing,[])

parseMessage :: String -> LogMessage
parseMessage s = maybe (Unknown s) id logm 
                 where
                   ws = words s
                   (msgt,ws1) = parseMsgType ws
                   (ts,ws2) = parseTime ws1
                   logm = LogMessage <$> msgt <*> ts <*> Just (unwords ws2)


parse :: String -> [LogMessage]
parse = (map parseMessage) . lines

-- Tree

-- Ex 2
insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert msg Leaf = Node Leaf msg Leaf 
insert m1@(LogMessage _ t1 _ ) (Node left m2@(LogMessage _ t2 _) right) 
    | t1 < t2 = Node (insert m1 left) m2 right
    | otherwise = Node left m2 (insert m1 right)
insert _ _ = Leaf

-- Ex 3
build :: [LogMessage] -> MessageTree
build = foldr insert Leaf

-- Ex 4
inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node l m r) = (inOrder l) ++ m : (inOrder r)

-- Ex 5
isRelevant :: LogMessage -> Bool
isRelevant (LogMessage (Error x) _ _) | x > 50 = True
isRelevant _ = False

getMsgString :: LogMessage -> String
getMsgString (LogMessage _ _ s) = s
getMsgString (Unknown s) = s

whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = (map getMsgString) . (filter isRelevant) . inOrder . build

-- Ex 6
-- Jean-Yves Girard ?




